// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

library Multipliers {
    // constants
    uint256 constant MULTIPLIER_1 = 3;
    uint256 constant MULTIPLIER_2 = 2;
    uint256 constant MULTIPLIER_3 = 1;

    function getMultiplier(uint256 timeLeft) internal pure returns (uint256) {
        if (timeLeft >= 3 minutes) {
            return MULTIPLIER_1;
        } else if (timeLeft >= 1 minutes) {
            return MULTIPLIER_2;
        } else {
            return MULTIPLIER_3;
        }
    }
}