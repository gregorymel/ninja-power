// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

interface INinjaPower {
    function startGame(address assetToken_, address fabric_, uint256 gameEndTime_) external;
    function sendToPool1From(address from, uint256 amount) external;
    function sendToPool2From(address from, uint256 amount) external;
    function withdraw(address to) external;
    function isGameEnded() external view returns(bool);
    function previewWithdrawal(address to) external view returns(uint256);
    function withdrawCore(address to, uint256 amount) external;
}