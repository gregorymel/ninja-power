// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import { Multipliers } from "./Multipliers.sol";
import { SafeERC20 } from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import { IERC20 } from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import { INinjaPower } from "./interfaces/INinjaPower.sol";
// import "./wCORE.sol";

contract NinjaPowerV2 is INinjaPower {
    using Multipliers for uint256;
    using SafeERC20 for IERC20;

    uint256 public constant MAINTENANCE_FEE_PERCENTAGE = 1;
    uint256 public constant REWARD_WITH_MAINTENANCE_FEE_PERCENTAGE = 100 - MAINTENANCE_FEE_PERCENTAGE;
    address public constant ADMIN_ADDRESS = 0x04e622503C1542bFdaE252f076603521FEa31E16;

    uint256 public pool1Points;
    uint256 public pool2Points;

    uint256 public pool1TotalBalance;
    uint256 public pool2TotalBalance;
    
    uint256 public gameEndTime;

    address public fabric;

    // uint256 public rewardAmount;

    IERC20 public assetToken;

    bool public gameStarted = false;
    
    mapping(address => uint256) public pool1Balances;
    mapping(address => uint256) public pool2Balances;
    mapping(address => uint256) public pool1PointsBalances;
    mapping(address => uint256) public pool2PointsBalances;

    // poolId: true - pool1, false - pool2
    event PoolReplenished(address indexed user, bool indexed poolId, uint256 assetAmount, uint256 pointsAmount);
    
    constructor() {}

    function startGame(address assetToken_, address fabric_, uint256 gameEndTime_) external {
        require(!gameStarted, "Game has already started.");

        assetToken = IERC20(assetToken_);

        gameStarted = true;
        gameEndTime = gameEndTime_;
        fabric = fabric_;
    }

    modifier gameStartedOnly() {
        require(gameStarted, "Game has not started yet.");
        _;
    }

    function _calculatePoints(uint256 amount) internal view returns(uint256) {
        uint256 timeLeft = gameEndTime - block.timestamp;
        uint256 multiplier = timeLeft.getMultiplier();
        return amount * multiplier;
    }

    function _sendToPool1(address from, uint256 amount) internal {
        require(block.timestamp < gameEndTime, "Game has ended.");
        require(amount > 0, "Amount should be greater than zero.");

        uint256 points = _calculatePoints(amount);
        pool1PointsBalances[from] += points;
        pool1Points += points;

        pool1Balances[from] += amount;
        pool1TotalBalance += amount;

        emit PoolReplenished(from, true, amount, points);

        assetToken.transferFrom(msg.sender, address(this), amount);
    }

    function _sendToPool2(address from, uint256 amount) internal {
        require(block.timestamp < gameEndTime, "Game has ended.");
        require(amount > 0, "Amount should be greater than zero.");

        uint256 points = _calculatePoints(amount);
        pool2PointsBalances[from] += points;
        pool2Points += points;

        pool2Balances[from] += amount;
        pool2TotalBalance += amount;

        emit PoolReplenished(from, false, amount, points);

        assetToken.transferFrom(msg.sender, address(this), amount);
    }

    function sendToPool1(uint256 amount) external gameStartedOnly {
        _sendToPool1(msg.sender, amount);
    }

    function sendToPool1From(address from, uint256 amount) external gameStartedOnly {
        _sendToPool1(from, amount);
    }
    
    function sendToPool2(uint256 amount) external gameStartedOnly {
        _sendToPool2(msg.sender, amount);
    }

    function sendToPool2From(address from, uint256 amount) external gameStartedOnly {
        _sendToPool2(from, amount);
    }

    function previewWithdrawal(address to) public view returns(uint256) {
        uint256 pool1Balance = pool1Balances[to];
        uint256 pool2Balance = pool2Balances[to];
        uint256 totalBalance = pool1Balance + pool2Balance;

        if (totalBalance == 0) return 0;

        uint256 reward = 0;
        if (pool1Points > pool2Points) {
            uint256 pool1PointsBalance = pool1PointsBalances[to];
            reward = pool2TotalBalance * pool1PointsBalance / pool1Points + pool1Balance;
        } else if (pool2Points > pool1Points) {
            uint256 pool2PointsBalance = pool2PointsBalances[to];
            reward = pool1TotalBalance * pool2PointsBalance / pool2Points + pool2Balance;
        } else {
            reward = totalBalance;
        }

        return reward * REWARD_WITH_MAINTENANCE_FEE_PERCENTAGE / 100;
    }

    function withdraw(address to) external {
        require(block.timestamp > gameEndTime, "Game has not ended yet.");

        uint256 withdrawableAmount = previewWithdrawal(to);
        require(withdrawableAmount > 0, "You have no balance to withdraw.");

        pool1Balances[to] = 0;
        pool2Balances[to] = 0;

        assetToken.safeTransfer(to, withdrawableAmount);
    }

    function withdrawCore(address to, uint256 amount) external {
        require(block.timestamp > gameEndTime, "Game has not ended yet.");
        require(msg.sender == fabric, "Only fabric!");

        require(amount > 0, "You have no balance to withdraw.");

        pool1Balances[to] = 0;
        pool2Balances[to] = 0;

        assetToken.safeTransfer(fabric, amount);
    }

    function withdrawFee(address to) external {
        require(block.timestamp > gameEndTime, "Game has not ended yet.");
        require(msg.sender == ADMIN_ADDRESS, "only admin");

        uint256 fee = (pool1TotalBalance + pool2TotalBalance) * MAINTENANCE_FEE_PERCENTAGE / 100;
        assetToken.safeTransfer(to, fee);
    }

    function rewardAmount() external view returns(uint256) {
        return (pool1TotalBalance + pool2TotalBalance) * REWARD_WITH_MAINTENANCE_FEE_PERCENTAGE / 100;
    }

    function isGameEnded() external view returns(bool) {
        return block.timestamp > gameEndTime;
    }
}