import { Contract, Signer, BigNumber, BigNumberish, utils } from 'ethers';
import type { Provider } from '@ethersproject/providers';
import { getContractAddressesForChainOrThrow } from './addresses';

import { default as NinjaPower_ABI } from '../abi/NinjaPowerV2.json';
import { default as GameFactory_ABI } from '../abi/GameFactory.json';
import { default as IERC20_ABI } from '../abi/IERC20.json';

import { GameFactory } from '../typechain-types/contracts/GameFactory';

import { Contracts, NinjaPowerExtended } from './types';

import { defineReadOnly } from '@ethersproject/properties';

/**
 * Get contract instances that target the Aurora mainnet
 * or a supported testnet. Throws if there are no known contracts
 * deployed on the corresponding chain.
 * @param chainId The desired chain id
 * @param signerOrProvider The ethers v5 signer or provider
 */
export function getContractsForChainOrThrow(chainId: number, signerOrProvider: Signer | Provider) {
    const addresses = getContractAddressesForChainOrThrow(chainId);

    const gameFactoryContract = new Contract(
        addresses.GameFactory,
        GameFactory_ABI,
        signerOrProvider
    );

    const auroraTokenContract = new Contract(addresses.AuroraToken, IERC20_ABI, signerOrProvider as Signer | Provider);

    return {
        gameFactoryContract,
        auroraTokenContract
    } as Contracts;
}

export async function getCurrentGame(gameFabricContract: GameFactory): Promise<NinjaPowerExtended> {
    const address = await gameFabricContract.getCurrentAddress();
    const signerOrProvider = gameFabricContract.signer || gameFabricContract.provider;
    const game = new Contract(address, NinjaPower_ABI, signerOrProvider) as NinjaPowerExtended;

    defineReadOnly(game, 'isActive', async () => {
        const code = await game.provider.getCode(game.address);

        if (code === '0x') return false;

        return true;
    });

    return game;
}

export async function getGameByIdx(gameFabricContract: GameFactory, idx: number): Promise<NinjaPowerExtended> {
    const address = await gameFabricContract.getAddress(idx);
    const signerOrProvider = gameFabricContract.signer || gameFabricContract.provider;
    const game = new Contract(address, NinjaPower_ABI, signerOrProvider) as NinjaPowerExtended;

    defineReadOnly(game, 'isActive', async () => {
        const code = await game.provider.getCode(game.address);

        if (code === '0x') return false;

        return true;
    });

    return game;
}