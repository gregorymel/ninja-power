import { BigNumberish, BigNumber } from 'ethers';
import { IERC20 } from '../typechain-types/@openzeppelin/contracts/token/ERC20/IERC20';
import { GameFactory } from '../typechain-types/contracts/GameFactory';
import { NinjaPowerV2 } from '../typechain-types/contracts/NinjaPowerV2';

export enum ChainId {
    CoreTestnet = 1115,
}

export interface ContractAddresses {
    GameFactory: string;
    AuroraToken: string;
}

interface IERC20ParseUnits {
    parseUnits(value: string): BigNumber;
    formatUnits(value: BigNumberish): string;
    formatUnitsWithDecimalPlaces(value: BigNumberish, decimalPlaces: number): string;
    getDecimals(): number;
}

export interface NinjaPowerExtended extends NinjaPowerV2 {
    isActive(): Promise<boolean>;
}

export interface Contracts {
    gameFactoryContract: GameFactory;
    auroraTokenContract: IERC20 & IERC20ParseUnits;
}
