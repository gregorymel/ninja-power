import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "hardhat-packager";
import "hardhat-abi-exporter";
import "@openzeppelin/hardhat-upgrades";
import "@nomiclabs/hardhat-etherscan";

import './tasks';

require('dotenv').config();

const AURORA_PRIVATE_KEY = process.env.AURORA_PRIVATE_KEY;
const AURORA_MAINNET_PRIVATE_KEY = process.env.AURORA_MAINNET_PRIVATE_KEY;
const AURORA_PLUS_URL = process.env.AURORA_PLUS_URL;

const config: HardhatUserConfig = {
  solidity: {
      version: "0.8.18",
      settings: {
        optimizer: {
          enabled: true,
          runs: 1000,
        },
      },
  },
  networks: {
      mainnet_aurora: {
          url: AURORA_PLUS_URL,
          accounts: [`0x${AURORA_MAINNET_PRIVATE_KEY}`],
          chainId: 1313161554
      },
      testnet_aurora: {
          url: 'https://testnet.aurora.dev',
          // url: 'https://aurora-testnet.infura.io/v3/040e4ba0ce5d4ca08c79203c7269990f',
          accounts: [`0x${AURORA_PRIVATE_KEY}`],
          // accounts: [`0x${AURORA_MAINNET_PRIVATE_KEY}`],
          chainId: 1313161555,
          gasPrice: 1 * 1000000000
      },
      testnet_core: {
          url: 'https://rpc.test.btcs.network/',
          accounts: [`0x${AURORA_PRIVATE_KEY}`],
          chainId: 1115
      }
  },
  etherscan: {
    apiKey: {
      mainnet_aurora: " ",
    },
    customChains: [
      {
        network: "mainnet_aurora",
        chainId: 1313161554,
        urls: {
          apiURL: "https://explorer.mainnet.aurora.dev/api",
          browserURL: "https://explorer.mainnet.aurora.dev/api"
        }
      }
    ]
  },
  packager: {
      contracts: [
        "NinjaPowerV2",
        "GameFactory",
        "IERC20",
      ],
      includeFactories: false,
  },
  abiExporter: {
      path: './abi',
      runOnCompile: true,
      flat: true,
      clear: true,
      only: ["NinjaPower", "GameFactory", "IERC20"],
      format: "json"
  }
};
export default config;
