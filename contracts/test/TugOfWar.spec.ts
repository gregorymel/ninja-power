import { ethers } from "hardhat";
import { Contract, Signer } from "ethers";
import { expect } from "chai";
import { time } from '@nomicfoundation/hardhat-network-helpers';
import { MockedAurora, NinjaPower } from "../typechain-types";

const SECONDS_IN_HOUR = 60 * 60;
const SECONDS_IN_MIN = 60;

describe("NinjaPower", function () {
  let owner: Signer;
  let player1: Signer;
  let player2: Signer;
  let assetToken: MockedAurora;
  let poolGame: NinjaPower;

  beforeEach(async function () {
    [owner, player1, player2] = await ethers.getSigners();

    // Deploy the ERC20 token contract
    const Aurora = await ethers.getContractFactory("MockedAurora");
    assetToken = await Aurora.deploy("Test Token", "TEST");

    // Deploy the pool game contract
    const PoolGame = await ethers.getContractFactory("NinjaPower");
    poolGame = await PoolGame.deploy();

    // Transfer some tokens to the players
    await assetToken.mint(await owner.getAddress(), ethers.utils.parseEther("100"));
    await assetToken.mint(await player1.getAddress(), ethers.utils.parseEther("1000"));
    await assetToken.mint(await player2.getAddress(), ethers.utils.parseEther("1000"));

    // Approve the pool game contract to spend tokens on behalf of the players
    await assetToken.connect(owner).approve(poolGame.address, ethers.utils.parseEther("100"));
    await assetToken.connect(player1).approve(poolGame.address, ethers.utils.parseEther("1000"));
    await assetToken.connect(player2).approve(poolGame.address, ethers.utils.parseEther("1000"));
  });

  it("should not allow sending tokens to pools before the game starts", async function () {
    await expect(poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has not started yet.");
    await expect(poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has not started yet.");
  });

  it("should allow sending tokens to pools after the game starts", async function () {
    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, ethers.utils.parseEther("100"), gameEndTime);
    await expect(poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.not.be.reverted;
    await expect(poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("100"))).to.not.be.reverted;
  });

  it("should not allow sending tokens to pools after the game ends", async function () {
    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, ethers.utils.parseEther("100"), gameEndTime);
    await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later
    await expect(poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has ended.");
    await expect(poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("100"))).to.be.revertedWith("Game has ended.");
  });

  it("should allow withdrawing tokens after the game ends", async function () {
    const player1Address = await player1.getAddress();
    const player2Address = await player2.getAddress();

    const player1BalanceBefore = await assetToken.balanceOf(player1Address);
    const player2BalanceBefore = await assetToken.balanceOf(player2Address);

    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, ethers.utils.parseEther("100"), gameEndTime);
    await poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"));
    await poolGame.connect(player2).sendToPool2(ethers.utils.parseEther("200"));
  
    await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later
  
    await expect(poolGame.connect(player1).withdraw(await player1.getAddress())).to.not.be.reverted;
  
    const player1BalanceAfter = await assetToken.balanceOf(player1Address);
    expect(player1BalanceAfter).to.equal(player1BalanceBefore);
  
    await expect(poolGame.connect(player2).withdraw(player2Address)).to.not.be.reverted;
  
    const rewardAmount = await poolGame.rewardAmount();
    const player2BalanceAfter = await assetToken.balanceOf(player2Address);
    expect(player2BalanceAfter).to.equal(player2BalanceBefore.add(rewardAmount));
  });

  it("should multiply points", async function () {
    const player1Address = await player1.getAddress();

    const value = ethers.utils.parseEther("1");
    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, ethers.utils.parseEther("100"), gameEndTime);
    
    await poolGame.connect(player1).sendToPool1(value);
    const points100 = await poolGame.pool1PointsBalances(player1Address);
    expect(points100).to.equal(value.mul(100));

    await ethers.provider.send("evm_increaseTime", [2 * 60 * 60]);

    await poolGame.connect(player1).sendToPool1(value);
    const points90 = (await poolGame.pool1PointsBalances(player1Address)).sub(points100);
    expect(points90).to.equal(value.mul(90));
  
    await ethers.provider.send("evm_increaseTime", [(21 * 60 + 50) * 60]);

    await poolGame.connect(player1).sendToPool1(value);
    const points2 = (await poolGame.pool1PointsBalances(player1Address)).sub(points100.add(points90));
    expect(points2).to.equal(value.mul(2));
  });

  it("should distribute rewards proportionally to points", async function () {
    const player1Address = await player1.getAddress();
    const player2Address = await player2.getAddress();

    const player1BalanceBefore = await assetToken.balanceOf(player1Address);
    const player2BalanceBefore = await assetToken.balanceOf(player2Address);

    const rewardAmount = ethers.utils.parseEther("100");
    const gameEndTime = await time.latest() + 23 * SECONDS_IN_HOUR + 55 * SECONDS_IN_MIN;
    await poolGame.connect(owner).startGame(assetToken.address, rewardAmount, gameEndTime);
    await poolGame.connect(player1).sendToPool1(ethers.utils.parseEther("100"));
    await poolGame.connect(player2).sendToPool1(ethers.utils.parseEther("200"));
  
    await ethers.provider.send("evm_increaseTime", [24 * 60 * 60]); // Increase time to 24 hours later

    await poolGame.withdraw(player1Address);
    await poolGame.withdraw(player2Address);

    const player1BalanceAfter = await assetToken.balanceOf(player1Address);
    const player2BalanceAfter = await assetToken.balanceOf(player2Address);
    const player1BalanceDiff = player1BalanceAfter.sub(player1BalanceBefore);
    const player2BalanceDiff = player2BalanceAfter.sub(player2BalanceBefore);

    expect(player1BalanceDiff).to.equal(rewardAmount.div(3));
    expect(player2BalanceDiff).to.equal(rewardAmount.mul(2).div(3));
  });
});