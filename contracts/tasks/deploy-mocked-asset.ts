import { task, types } from 'hardhat/config';

task("deploy:MockedAsset", "Deploys mocked wCore ERC20 token")
  .setAction(async (taskArgs, hre) => {
    // const [deployer] = await hre.ethers.getSigners();

    // const Aurora = await hre.ethers.getContractFactory("MockedCore");
    const Aurora = await hre.ethers.getContractFactory("WCORE");
    const auroraToken = await Aurora.deploy();

    console.log(`WCORE deployed at ${auroraToken.address}`);
  });