import { Contract } from 'ethers';
import { readFileSync, writeFileSync } from 'fs';
import { task } from 'hardhat/config';
import { join } from 'path';

task("create-game", "Create game")
  .addParam("token", "Address of the Aurora token")
  .addParam("rewardAmount", "Reward amount")
  .setAction(async (taskArgs, hre) => {
    const [deployer,] = await hre.ethers.getSigners();
    const { chainId } = await hre.ethers.provider.getNetwork();
    const addressesPath = join(__dirname, '../src/addrs.json');
    const addresses = JSON.parse(readFileSync(addressesPath, 'utf8'));

    // const auroraTokenContract = new Contract()

    const GameFactory = await hre.ethers.getContractFactory("GameFactory");
    const gameFactoryContract = await GameFactory.attach(addresses[chainId].GameFactory);

    const rewardAmount = hre.ethers.utils.parseEther(taskArgs.rewardAmount);

    // const tx_ = await auroraTokenContract.approve(addresses[chainId].GameFactory, rewardAmount);
    // await tx_.wait();

    const tx = await gameFactoryContract.createContract(taskArgs.token, rewardAmount);
    await tx.wait();

    const newAddress = await gameFactoryContract.getCurrentAddress();
    console.log("New game deployed to:", newAddress);
  });