import { task, types } from 'hardhat/config';

task("deploy:NinjaPower", "Deploys the PoolGame contract and starts the game")
  .setAction(async (taskArgs, hre) => {
    const [deployer] = await hre.ethers.getSigners();
    console.log("Deployer ", deployer.address);

    const NinjaPower = await hre.ethers.getContractFactory("NinjaPowerV2");
    console.log("Here");
    const ninjaPower = await NinjaPower.deploy();

    console.log(`NinjaPower deployed at ${ninjaPower.address}`);
  });