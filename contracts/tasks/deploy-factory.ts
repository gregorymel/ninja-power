import { readFileSync, writeFileSync } from 'fs';
import { task } from 'hardhat/config';
import { join } from 'path';

task("deploy:GameFactory", "Deploys the GameFactory contract and saves its address to a JSON file")
  .addParam("masterAddress", "Address of the master contract")
  .addParam("token", "Address of the Aurora token")
  .addParam("rewardAmount", "Reward amount")
  .setAction(async (taskArgs, hre) => {
    const [deployer] = await hre.ethers.getSigners();
    console.log("Deployer ", deployer.address);

    const GameFactory = await hre.ethers.getContractFactory("GameFactory");
    const rewardAmount = hre.ethers.utils.parseEther(taskArgs.rewardAmount);
    const gameFactory = await hre.upgrades.deployProxy(GameFactory, [taskArgs.masterAddress, taskArgs.token, rewardAmount]);
    // const gameFactory = await GameFactory.deploy(taskArgs.masterAddress, taskArgs.token, rewardAmount);
    await gameFactory.deployed();

    console.log("GameFactory deployed to:", gameFactory.address);

    const { chainId } = await hre.ethers.provider.getNetwork();
    const addressesPath = join(__dirname, '../src/addrs.json');
    const addresses = JSON.parse(readFileSync(addressesPath, 'utf8'));
    const contracts = {
        GameFactory: gameFactory.address
    };

    addresses[chainId] = {
      ...addresses[chainId],
      ...contracts
    };

    writeFileSync(addressesPath, JSON.stringify(addresses, null, 2));

    console.log("GameFactory address saved to addresses.json");
  });