import { readFileSync, writeFileSync } from 'fs';
import { task } from 'hardhat/config';
import { join } from 'path';

task("upgrade:GameFactory", "Deploys the GameFactory contract and saves its address to a JSON file")
  .setAction(async (taskArgs, hre) => {
    const [deployer] = await hre.ethers.getSigners();
    console.log("Deployer ", deployer.address);

    const GameFactory = await hre.ethers.getContractFactory("GameFactory");

    // console.log("GameFactory deployed to:", gameFactory.address);

    const { chainId } = await hre.ethers.provider.getNetwork();
    const addressesPath = join(__dirname, '../src/addrs.json');
    const addresses = JSON.parse(readFileSync(addressesPath, 'utf8'));

    const gameFactory = await hre.upgrades.upgradeProxy(addresses[chainId].GameFactory, GameFactory);

    console.log("GameFactory upgraded");
  });