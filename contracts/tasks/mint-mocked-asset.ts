import { task, types } from 'hardhat/config';
import { join } from 'path';
import { readFileSync, writeFileSync } from 'fs';


task("mint:MockedAsset", "Deploys mocked Aurora ERC20 token")
  .addParam("to", "dst address")
  .setAction(async (taskArgs, hre) => {
    const [deployer,] = await hre.ethers.getSigners();
    const { chainId } = await hre.ethers.provider.getNetwork();
    const addressesPath = join(__dirname, '../src/addrs.json');
    const addresses = JSON.parse(readFileSync(addressesPath, 'utf8'));

    const Aurora = await hre.ethers.getContractFactory("MockedCore");
    const auroraToken = await Aurora.attach(addresses[chainId].AuroraToken);

    const tx = await auroraToken.mint(taskArgs.to, hre.ethers.utils.parseEther("1000000"));
    await tx.wait();

    const balance = await auroraToken.balanceOf(taskArgs.to);

    console.log(`Aurora monted to ${auroraToken.address} ${balance}`);
  });