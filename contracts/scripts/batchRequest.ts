import { ethers } from "hardhat";
import { getContractsForChainOrThrow, getGameByIdx } from "../src";
import { GameFactory } from "../typechain-types";
import { getIntervalStats } from "../src/events";


// address(uint(keccak256(abi.encodePacked(
//     hex'ff',
//     factory, //is Address
//     keccak256(abi.encodePacked(token0, token1)), //token0 and token1 are addresses
//     hex'00fb7f630766e6a796048ea87d01acd3068e8ff67d078148a3fa3f4a84f69bd5' // init code hash
// ))));

// function create2(address, salt, initCode) {

// }

// function getLogFilter(addresses: string[], idx: number) {
//     return {
//         address: [this.contracts.b4bContract.address, this.contracts.uniqueIdentityContract.address],
//         topics: [
            
//         ],
//     };
// }

async function main() {
    const [deployer] = await ethers.getSigners();
    const chainId = await deployer.getChainId();
    
    // const provider = deployer.provider!;


    // const url = "https://mainnet.aurora.dev/6LWhPRjfjywdzMh82CLHVkCaZHqKhDuEyHtiEXP5C1X";
    const url = "https://aurora-mainnet.infura.io/v3/040e4ba0ce5d4ca08c79203c7269990f";
    const batchProvider = new ethers.providers.JsonRpcBatchProvider(url);
    // const contracts = await getContractsForChainOrThrow(chainId, batchProvider);

    // const filter = contracts.gameFactoryContract.
    
    // const userAddress = "0x6a2744ec9fb3b8deea75825e8de5f7be58beb633";
    
    // const result25 = await getIntervalStats(batchProvider, 25, 25);
    // console.log("here25");
    // const result26 = await getIntervalStats(batchProvider, 26, 26);
    // console.log("here26");
    const result126 = await getIntervalStats(batchProvider, 1, 26);
    // console.log(result126.addresses["0xe473b6621a9AEd651d5962012ce8695171258a90"]);

    // console.log(Object.keys(result25.addresses));
    // console.log(Object.keys(result26.addresses));

    // const set = new Set([...Object.keys(result25.addresses), ...Object.keys(result26.addresses)]);

    // const parsedLogs = logs.map((log: any) => game.interface.parseLog(log));
    // console.log(parsedLogs);
}

main()
    .then(() => {
        console.log("Finished!");
    });