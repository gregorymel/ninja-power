import { ethers } from "hardhat";
import { getContractsForChainOrThrow, getGameByIdx, getCurrentGame } from "../src";
import { GameFactory } from "../typechain-types";
import { getIntervalStats } from "../src/events";

async function main() {
    const [deployer] = await ethers.getSigners();
    const chainId = await deployer.getChainId();

    // const url = "https://mainnet.aurora.dev/6LWhPRjfjywdzMh82CLHVkCaZHqKhDuEyHtiEXP5C1X";
    // const url = "https://aurora-mainnet.infura.io/v3/040e4ba0ce5d4ca08c79203c7269990f";
    const url = 'https://rpc.test.btcs.network/';
    const batchProvider = new ethers.providers.JsonRpcProvider(url);
    
    const contracts = getContractsForChainOrThrow(chainId, deployer);
    const fabric = contracts.gameFactoryContract;
    const aurora = contracts.auroraTokenContract;

    // const WCORE = await ethers.getContractFactory("WCORE");
    // const wcore = WCORE.attach('0xc5ddF9D350E1BcA45B7D462D711C08Bf4925b59d');

    // const amount = ethers.utils.parseEther('0.01');
    // // console.log(amount);
    // const tx1 = await wcore.deposit({value: amount});
    // await tx1.wait();

    // const tx2 = await wcore.withdraw(amount);
    // await tx2.wait();


    const master = await fabric.masterContract();
    console.log("Master ", master);

    const game = await getCurrentGame(fabric);
    console.log("Game ", game.address);

    const curAddr = await fabric.getCurrentAddress();
    console.log("Cur addr", curAddr);

    const curId = await fabric.getCurrentGameIdx();
    console.log("Cur ID", curId);

    console.log("Active, ", await game.isActive());



    const amount = ethers.utils.parseEther('0.01');
    console.log(amount);

    // const tx1 = await aurora.approve(curAddr, amount);
    // await tx1.wait();

    // const tx2 = await fabric.sendToPool2(amount, { value: amount });
    // await tx2.wait();


    // const balance = await game.pool2PointsBalances(deployer.address);
    // console.log(balance);

    const withd = await fabric.previewWithdrawal(deployer.address);
    console.log(withd);


    // const amount1 = await game.previewWithdrawal(account);
    // const amount2 = await fabric.previewWithdrawal(account);
    // console.log(amount2);

    const tx = await fabric.withdraw(deployer.address);
    await tx.wait();

    // const rec = await tx.wait();
    // console.log(rec.transactionHash);
    // const games = await fabric.games(account, 1);
    // console.log(games);
}

main()
    .then(() => {
        console.log("Finished!");
    });