import { ethers } from "hardhat";
import { getContractsForChainOrThrow, getGameByIdx } from "../src";
import { GameFactory } from "../typechain-types";
import { getIntervalStats } from "../src/events";


// address(uint(keccak256(abi.encodePacked(
//     hex'ff',
//     factory, //is Address
//     keccak256(abi.encodePacked(token0, token1)), //token0 and token1 are addresses
//     hex'00fb7f630766e6a796048ea87d01acd3068e8ff67d078148a3fa3f4a84f69bd5' // init code hash
// ))));

// function create2(address, salt, initCode) {

// }

// function getLogFilter(addresses: string[], idx: number) {
//     return {
//         address: [this.contracts.b4bContract.address, this.contracts.uniqueIdentityContract.address],
//         topics: [
            
//         ],
//     };
// }

async function main() {
    const [deployer] = await ethers.getSigners();
    const chainId = await deployer.getChainId();

    const url = "https://mainnet.aurora.dev/6LWhPRjfjywdzMh82CLHVkCaZHqKhDuEyHtiEXP5C1X";
    // const url = "https://aurora-mainnet.infura.io/v3/040e4ba0ce5d4ca08c79203c7269990f";
    const batchProvider = new ethers.providers.JsonRpcProvider(url);
    
    const contracts = getContractsForChainOrThrow(chainId, batchProvider);
    const fabric = contracts.gameFactoryContract;
    const aurora = contracts.auroraTokenContract;

    const from = "0xe473b6621a9AEd651d5962012ce8695171258a90";
    // const from = "0x6a2744ec9fb3b8deea75825e8de5f7be58beb633";

    for (let i = 1; i <= 26; i++) {
        const game = await getGameByIdx(fabric, i);
        const balnce1 = await game.pool1PointsBalances(from);
        const balnce2 = await game.pool2PointsBalances(from);
        console.log(i, balnce1, balnce2);
    }
}

main()
    .then(() => {
        console.log("Finished!");
    });