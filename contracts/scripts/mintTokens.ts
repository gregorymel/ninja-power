import { ethers } from "hardhat";
import { getContractsForChainOrThrow, getGameByIdx } from "../src";
import { GameFactory } from "../typechain-types";


async function main() {
    const [deployer] = await ethers.getSigners();
    const tx = await deployer.sendTransaction({
        to: "0xF1E7e7eeB52Ab4a382ad12CF5EAd07a84AEf5F8f",
        value: ethers.utils.parseEther("0.001")
    });

    await tx.wait();
}

main()
    .then(() => {
        console.log("Finished!");
    });