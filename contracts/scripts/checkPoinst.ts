import { ethers } from "hardhat";
import { getContractsForChainOrThrow, getGameByIdx } from "../src";
import { GameFactory } from "../typechain-types";


async function main() {
    const [deployer] = await ethers.getSigners();
    const chainId = await deployer.getChainId();
    
    const GameFactory = await ethers.getContractFactory("GameFactory");
    const factory = GameFactory.attach("0x5939943946031e75cC7642097ecB2FB37cA24943");

    // const gameId = await factory.getCurrentGameIdx();
    // console.log(gameId);
    const newMasterContract = "0xDf17482fA13D706040E1120D3281abf8F8c48FC3";
    const tx = await factory.checkpoint(newMasterContract);

    await tx.wait();

    // console.log("Finished!");
}

main()
    .then(() => {
        console.log("Finished!");
    });