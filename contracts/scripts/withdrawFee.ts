import { ethers } from "hardhat";
import { getContractsForChainOrThrow, getGameByIdx } from "../src";
import { GameFactory } from "../typechain-types";
import { getIntervalStats } from "../src/events";

// import { default as TugOfWar_ABI } from '../abi/TugOfWarV2.json';

async function main() {
    const [deployer] = await ethers.getSigners();
    const chainId = await deployer.getChainId();

    const url = "https://mainnet.aurora.dev/6LWhPRjfjywdzMh82CLHVkCaZHqKhDuEyHtiEXP5C1X";
    // const url = "https://aurora-mainnet.infura.io/v3/040e4ba0ce5d4ca08c79203c7269990f";
    const batchProvider = new ethers.providers.JsonRpcProvider(url);
    
    const contracts = getContractsForChainOrThrow(chainId, deployer);
    const fabric = contracts.gameFactoryContract;
    const aurora = contracts.auroraTokenContract;

    const game = await getGameByIdx(fabric, 30);
    console.log("Game ", game.address);
    // const account = "0x587ed0683581fE5bD127A3CE4450cFCe7E00629c";

    // const amount1 = await game.previewWithdrawal(account);
    // const amount2 = await fabric.previewWithdrawal(account);
    // console.log(amount2);

    const tx = await game.withdrawFee(deployer.address);
    const rec = await tx.wait();
    // console.log(rec.transactionHash);
    // const games = await fabric.games(account, 1);
    // console.log(games);
}

main()
    .then(() => {
        console.log("Finished!");
    });