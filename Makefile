build:
	@echo "Buliding Docker image..."
	docker build -t ninja-power-frontend .

build-amd:
	@echo "Buliding Docker image..."
	docker build -t ninja-power-frontend --platform linux/amd64 .

save-amd: build-amd
	@echo "Saving Docker image to file..."
	docker save ninja-power-frontend -o ninja-power-frontend.tar


run: clean build
	@echo "Running Docker container..."
	docker run -d -p 80:80 --name ninja-power-frontend ninja-power-frontend && open http://localhost

clean:
	@echo "Cleaning up..."
	rm -f ninja-power-frontend.tar
	rm -rf ./dist
	docker rm -f ninja-power-frontend

send: save-amd
	@echo "Sending docker-image tarball to remote server..."
	scp -i ~/contabo-server ninja-power-frontend.tar kezhik@89.117.52.134:~

deploy: send
	@echo "Extracting tarball and running container in remote server..."
	ssh -i ~/contabo-server kezhik@89.117.52.134 "docker rm -f ninja-power-frontend && docker load -i ~/ninja-power-frontend.tar && docker run -d -p 80:80 -p 443:443 --name ninja-power-frontend ninja-power-frontend"
