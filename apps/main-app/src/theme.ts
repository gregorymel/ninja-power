import { ThemeConfig, theme as antdTheme } from 'antd';

export const theme: ThemeConfig = {
  algorithm: [antdTheme.darkAlgorithm],
  token: {
    fontFamily: 'Inter, sans-serif',
    colorPrimary: '#000',
    colorBgBase: '#fff',
    colorTextBase: '#000',
    borderRadius: 0,
    fontSize: 16,
    fontSizeHeading3: 24,
  },
  components: {
    Button: {
      fontSize: 16,
      borderRadius: 0,
      colorBorder: '#000',
      paddingContentHorizontal: 32,
    },
    Menu: {
      colorItemTextSelected: '#fff',
      fontSize: 24,
      controlHeightLG: 64,
    },
    Layout: {
      colorBgHeader: '#DDF0F9',
      controlHeight: 44,
    },
    Input: {
      colorBorder: '#565656',
    },
    Table: {
      colorTextHeading: '#fff',
    },
    Tooltip: {
      colorTextLightSolid: '#000000bb',
    },
    Divider: {
      colorSplit: '#00000022',
    },
  },
};
