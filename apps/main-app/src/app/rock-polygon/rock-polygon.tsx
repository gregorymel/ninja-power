import styles from './rock-polygon.module.less';
import { RockLeftImage } from '@ninja-power/ui-icons';
import classNames from 'classnames';

export interface RockPolygonProps {
  black?: boolean;
}

export function RockPolygon({ black }: RockPolygonProps) {
  return (
    <div className={classNames(styles['container'], black && styles['black'])}>
      <RockLeftImage className={styles['rock']} />
    </div>
  );
}

export default RockPolygon;
