import NavIcon from '../nav-icon/nav-icon';
import { Row } from 'antd';
import React, { useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { ERoutes } from '../../routes';
import styles from './navbar.module.less';

interface NavBarItem {
  to: string;
  icon: React.ReactNode;
  external?: boolean;
}

export interface NavbarProps {
  items: NavBarItem[];
}

export function Navbar({ items }: NavbarProps) {
  const { pathname } = useLocation();
  const currentRoutePathname = useMemo(() => {
    for (const routeKey of Object.keys(ERoutes)) {
      const routePathname = (ERoutes as any)[routeKey];
      if (routePathname === pathname) {
        return routePathname;
      }
    }

    return null;
  }, [pathname]);

  return (
    <Row wrap={false} justify={'center'} className={styles['container']}>
      {items.map(({ to, icon, external }) => (
        <NavIcon
          to={to}
          icon={icon}
          key={to}
          active={currentRoutePathname === to}
          external={external}
        />
      ))}
    </Row>
  );
}

export default Navbar;
