import { Col, Divider, Modal, Row, Typography } from 'antd';
import styles from './game-ended-modal.module.less';
import { useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';
import { digitSplitter } from '@ninja-power/utils';
import WithdrawController from '../withdraw-controller/withdraw-controller';
import { useEffect } from 'react';
import { currencyName } from '@ninja-power/config';

const IS_TOTAL_COLLECTED_SHOWN = false;
const IS_TOTAL_POINTS_SHOWN = true;
let tries = 0;

/* eslint-disable-next-line */
export interface GameEndedModalProps {}

export const GameEndedModalController = observer(
  (props: GameEndedModalProps) => {
    const {
      gameStore,
      appController,
      winnersStore,
      blackTeamStore,
      whiteTeamStore,
    } = useStore();

    const whiteTeamPoints = winnersStore.lastGameResult?.white;
    const blackTeamPoints = winnersStore.lastGameResult?.black;

    const isWhiteTeamWon = whiteTeamPoints > blackTeamPoints;
    const isBlackTeamWon = whiteTeamPoints < blackTeamPoints;
    const isDraw =
      !isNaN(whiteTeamPoints) &&
      !isNaN(blackTeamPoints) &&
      whiteTeamPoints === blackTeamPoints;
    const isNobodyParticipated =
      isNaN(whiteTeamPoints) && isNaN(blackTeamPoints);

    useEffect(() => {
      winnersStore.updateLastGameResult();
    }, []);

    useEffect(() => {
      if (
        gameStore.nextGameDuration?.milliseconds <= 1000 &&
        tries++ % 3 === 0
      ) {
        appController.revalidateGameState();
      }
    }, [appController, gameStore, gameStore.nextGameDuration]);

    return (
      <Modal open footer={<></>} closeIcon={<></>}>
        <div className={styles['container']}>
          <Row justify={'center'}>
            <Typography.Title level={2}>
              {isWhiteTeamWon && 'White Team Won!'}
              {isBlackTeamWon && 'Black Team Won!'}
              {isDraw && 'Draw'}
              {isNobodyParticipated && 'No participants'}
            </Typography.Title>
          </Row>
          {!isNobodyParticipated && IS_TOTAL_COLLECTED_SHOWN && (
            <>
              <Row justify={'center'}>
                <Typography.Title level={4}>Total collected:</Typography.Title>
              </Row>
              <Row justify={'center'}>
                <Typography.Title level={3}>
                  {digitSplitter(appController.bagAmount)}
                  &nbsp;{currencyName}
                </Typography.Title>
              </Row>
            </>
          )}
          {!isNobodyParticipated && IS_TOTAL_POINTS_SHOWN && (
            <>
              <Row gutter={16} justify={'center'}>
                <Col>
                  <Typography.Text>White team:</Typography.Text>
                </Col>
                <Col>
                  <Typography.Text>Black team:</Typography.Text>
                </Col>
              </Row>
              <Row justify={'center'} gutter={16}>
                <Col>
                  <Typography.Text>
                    <b>
                      {digitSplitter(whiteTeamPoints?.toFixed(2))}
                      &nbsp;Points
                    </b>
                  </Typography.Text>
                </Col>
                <Col>
                  <Typography.Text>
                    <b>
                      {digitSplitter(blackTeamPoints?.toFixed(2))}
                      &nbsp;Points
                    </b>
                  </Typography.Text>
                </Col>
              </Row>
            </>
          )}
          <WithdrawController />
          <Row justify={'center'}>
            <Divider type={'horizontal'} />
          </Row>
          <Row justify={'center'}>
            <Typography.Text type={'secondary'}>
              {gameStore.nextGameDuration
                ? 'Next game in:'
                : 'The next game starts soon...'}
            </Typography.Text>
          </Row>
          <Row justify={'center'}>
            <Typography.Title>
              {gameStore.nextGameDuration?.toFormat('hh:mm:ss')}
            </Typography.Title>
          </Row>
        </div>
      </Modal>
    );
  },
);

export default GameEndedModalController;
