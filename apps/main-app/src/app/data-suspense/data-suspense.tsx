import React, { PropsWithChildren } from 'react';
import { useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';

export interface DataSuspenseProps extends PropsWithChildren {
  fallBack: React.ReactNode;
}

export const DataSuspense = observer(
  ({ children, fallBack }: DataSuspenseProps) => {
    const {
      initialLoadingStore: { isLoading },
    } = useStore();

    return <>{isLoading ? fallBack : children}</>;
  },
);

export default DataSuspense;
