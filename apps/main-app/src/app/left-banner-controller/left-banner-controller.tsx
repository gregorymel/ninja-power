import styles from './left-banner-controller.module.less';
import { LeftBanner } from '../app-content/main-content/left-banner';
import React from 'react';
import { useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';

/* eslint-disable-next-line */
export interface LeftBannerControllerProps {}

export const LeftBannerController = observer(
  (props: LeftBannerControllerProps) => {
    const {
      gameStore: { stopsInDuration, rateChangeIn, currentRateValue, gameId },
    } = useStore();

    return (
      <LeftBanner
        className={styles['left-banner-container']}
        stopsInDuration={stopsInDuration}
        rateChangeInDuration={rateChangeIn}
        currentRateValue={currentRateValue}
        gameId={gameId}
      />
    );
  },
);

export default LeftBannerController;
