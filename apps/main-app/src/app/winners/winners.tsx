import styles from './winners.module.less';
import { Row, Table } from 'antd';
import { digitSplitter } from '@ninja-power/utils';
import { WinnersItem } from '@ninja-power/controllers';
import { currencyName } from '@ninja-power/config';

const columns = [
  {
    title: (
      <Row className={styles['table-header-text']}>
        <b>Game#</b>
      </Row>
    ),
    dataIndex: 'number',
    render: (v: any) => `#${v}`,
  },
  {
    title: (
      <div>
        <Row className={styles['table-header-text']}>
          <b>White</b>
        </Row>
        <Row className={styles['table-header-caption']}>Points</Row>
      </div>
    ),
    dataIndex: 'white',
    render: (v: any, { black }: any) => {
      const rounded = Math.round(Number(v));
      const split = digitSplitter(rounded);

      return <>{v > black ? <b>{split}</b> : split}</>;
    },
  },
  {
    title: (
      <div>
        <Row className={styles['table-header-text']}>
          <b>Black</b>
        </Row>
        <Row className={styles['table-header-caption']}>Points</Row>
      </div>
    ),
    dataIndex: 'black',
    render: (v: any, { white }: any) => {
      const rounded = Math.round(Number(v));
      const split = digitSplitter(rounded);

      return <>{v > white ? <b>{split}</b> : digitSplitter(rounded)}</>;
    },
  },
  {
    title: (
      <div>
        <Row className={styles['table-header-text']}>
          <b>White tokens</b>
        </Row>
        <Row className={styles['table-header-caption']}>{currencyName}</Row>
      </div>
    ),
    dataIndex: 'whiteTokens',
    render: (v: any) => digitSplitter(v),
  },
  {
    title: (
      <div>
        <Row className={styles['table-header-text']}>
          <b>Black tokens</b>
        </Row>
        <Row className={styles['table-header-caption']}>{currencyName}</Row>
      </div>
    ),
    dataIndex: 'blackTokens',
    render: (v: any) => digitSplitter(v),
  },
];

export interface WinnersProps {
  winners: WinnersItem[];
}

export function Winners({ winners }: WinnersProps) {
  return (
    <div className={styles['container']}>
      <Table
        columns={columns}
        dataSource={winners.map((v) => ({ ...v, key: v.number }))}
        pagination={false}
      />
    </div>
  );
}

export default Winners;
