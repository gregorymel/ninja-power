import { Button, notification } from 'antd';
import { useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';
import { currencyName } from '@ninja-power/config';

/* eslint-disable-next-line */
export interface WithdrawButtonProps {}

export const WithdrawButton = observer((props: WithdrawButtonProps) => {
  const { gameStore } = useStore();

  const isWithdrawable = gameStore.amountToWithdraw > 0;

  return (
    <Button
      type={'primary'}
      onClick={async () => {
        try {
          await gameStore.onWithdraw();
          await gameStore.updateAmountToWithdraw();
        } catch (e) {
          if (e instanceof Error) {
            notification.error({
              message: 'Something went wrong',
              description: e.message,
            });
          } else {
            notification.error({
              message: 'Something went wrong',
              description: String(e),
            });
          }
          return;
        }
        notification.success({
          message: 'Withdraw successful!',
        });
      }}
      loading={gameStore.isWithdrawButtonLoading}
      disabled={!isWithdrawable}
    >
      {isWithdrawable
        ? `Withdraw ${gameStore.amountToWithdraw.toFixed(2)} ${currencyName}`
        : 'Nothing to withdraw'}
    </Button>
  );
});

export default WithdrawButton;
