import AppHeaderController from './app-header-controller/app-header-controller';
import AppContentController from './app-content/app-content-controller';
import { Layout } from 'antd';
import { Loading } from '@ninja-power/ui-components';
import DataSuspense from './data-suspense/data-suspense';
import { useStore } from '@ninja-power/controllers';
import { useEffect } from 'react';
import { observer } from 'mobx-react-lite';

export const App = observer(() => {
  const { appController, initialLoadingStore } = useStore();
  useEffect(() => {
    appController.init();
  }, []);

  return (
    <DataSuspense
      fallBack={
        <Loading
          isLoaderHidden={initialLoadingStore.isLoaderHidden}
          message={initialLoadingStore.message}
        />
      }
    >
      <Layout>
        <AppHeaderController />
        <AppContentController />
      </Layout>
    </DataSuspense>
  );
});

export default App;
