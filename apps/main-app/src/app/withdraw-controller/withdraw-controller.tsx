import styles from './withdraw-controller.module.less';
import WithdrawButton from '../withdraw-button/withdraw-button';
import { Button, Divider, Row, Typography } from 'antd';
import { digitSplitter } from '@ninja-power/utils';
import { useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { interval, noop, tap } from 'rxjs';
import { currencyName } from '@ninja-power/config';

const IS_PROMO_PERIOD = false;

/* eslint-disable-next-line */
export interface WithdrawControllerProps {
  hasDivider?: boolean;
}

export const WithdrawController = observer(
  ({ hasDivider = true }: WithdrawControllerProps) => {
    const { gameStore, whiteTeamStore, blackTeamStore, web3Store } = useStore();

    const isWhiteTeamWon = whiteTeamStore.points > blackTeamStore.points;
    const isBlackTeamWon = whiteTeamStore.points < blackTeamStore.points;
    const isDraw = whiteTeamStore.points === blackTeamStore.points;

    let wonTeamPointsShare = null;
    if (isWhiteTeamWon) {
      wonTeamPointsShare = whiteTeamStore.pointsShare;
    } else if (isBlackTeamWon) {
      wonTeamPointsShare = blackTeamStore.pointsShare;
    }
    let rewardBias = 0;
    if (IS_PROMO_PERIOD) {
      rewardBias = whiteTeamStore.yourBalance + blackTeamStore.yourBalance;
    }

    const isWithdrawable = gameStore.amountToWithdraw > 0;

    useEffect(() => {
      if (web3Store.isMetamaskInstalled) {
        gameStore.updateAmountToWithdraw();

        const stream$ = interval(5000).pipe(
          tap(() => {
            gameStore.fetchAmountToWithdraw();
          }),
        );
        const subscription = stream$.subscribe(noop);

        return () => {
          subscription.unsubscribe();
        };
      }
    }, []);

    return (
      <div className={styles['container']}>
        {!!wonTeamPointsShare && isWithdrawable && (
          <>
            <Row justify={'center'}>
              <Typography.Title level={4}>Your Reward:</Typography.Title>
            </Row>
            <Row justify={'center'}>
              <Typography.Title level={3}>
                {digitSplitter(gameStore.amountToWithdraw.toFixed(2))}
                &nbsp;{currencyName}
              </Typography.Title>
            </Row>
          </>
        )}
        {web3Store.isMetamaskInstalled && (
          <>
            {hasDivider && <Divider type={'horizontal'} />}
            <Row justify={'center'}>
              <WithdrawButton />
            </Row>
          </>
        )}
        {!web3Store.isMetamaskInstalled && (
          <Button disabled>Not logged in</Button>
        )}
      </div>
    );
  },
);

export default WithdrawController;
