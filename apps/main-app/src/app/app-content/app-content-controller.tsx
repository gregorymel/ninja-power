import styles from './app-content.module.less';
import { Layout } from 'antd';
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';
import { MainContent } from './main-content/main-content';
import { ERoutes } from '../../routes';
import { TableContentController } from './table-content-controller/table-content-controller';
import classNames from 'classnames';
import { useStore } from '@ninja-power/controllers';
import GameEndedModalController from '../game-ended-modal/game-ended-modal-controller';
import { observer } from 'mobx-react-lite';

/* eslint-disable-next-line */
export interface AppContentProps {}

export const AppContentController = observer((props: AppContentProps) => {
  const { pathname } = useLocation();
  const isGamePage = pathname === ERoutes.MAIN;
  const {
    web3Store: { isMetamaskInstalled },
    gameStore: { isGameStopped },
  } = useStore();

  return (
    <Layout.Content
      className={classNames(
        styles['container'],
        isGamePage && styles['container-app-component-store'],
      )}
    >
      <Routes>
        <Route
          path={ERoutes.MAIN}
          element={
            isGameStopped ? <GameEndedModalController /> : <MainContent />
          }
        />
        <Route
          path={`${ERoutes.TABLE}/*`}
          element={<TableContentController />}
        />
        <Route path={'*'} element={<Navigate to={ERoutes.MAIN} />} />
      </Routes>
    </Layout.Content>
  );
});

export default AppContentController;
