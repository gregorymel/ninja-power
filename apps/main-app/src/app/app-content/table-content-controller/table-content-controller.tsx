import { GameStops, MenuPages, Rules } from '@ninja-power/ui-components';
import { Col, Row, Typography } from 'antd';
import styles from './table-content-controller.module.less';
import CalculateRate from '../../calculate-rate/calculate-rate';
import { useCallback, useRef, useState } from 'react';
import { ERoutes } from '../../../routes';
import { useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';
import WithdrawController from '../../withdraw-controller/withdraw-controller';
import WinnersController from '../../winners-controller/winners-controller';
import { currencyName } from '@ninja-power/config';

export const TableContentController = observer(() => {
  const contentRef = useRef<HTMLDivElement>(null);
  const [isFullInfoShown, setIsFullInfoShown] = useState(false);
  const { gameStore, gameTableStore } = useStore();

  const onYouSendChange = useCallback((v: string) => {
    gameTableStore.updateYouSendValue(v);
  }, []);

  const onTeamGetsChange = useCallback((v: string) => {
    gameTableStore.updateTeamGetsValue(v);
  }, []);

  return (
    <div className={styles['container']}>
      <Row className={styles['app-component-store-stops-container']}>
        <GameStops
          isCompact={false}
          stopsInDuration={gameStore.stopsInDuration}
          gameId={gameStore.gameId}
        />
      </Row>
      <Row gutter={120} wrap={false}>
        <Col flex={'400px'}>
          <MenuPages
            items={[
              {
                content: <Rules multiplierCoefLink={ERoutes.TABLE_CALCULATE} />,
                pathname: ERoutes.TABLE_RULES,
                title: 'Rules',
              },
              {
                content: <WinnersController />,
                pathname: ERoutes.TABLE_WINNERS,
                title: 'History',
              },
              {
                content: (
                  <>
                    <Row>
                      <Typography.Paragraph>
                        Here you can withdraw {currencyName} tokens from
                        previous games.
                      </Typography.Paragraph>
                    </Row>
                    <Row>
                      <WithdrawController hasDivider={false} />
                    </Row>
                  </>
                ),
                pathname: ERoutes.TABLE_WITHDRAW,
                title: 'Withdraw',
              },
              {
                content: (
                  <CalculateRate
                    youSendValue={gameTableStore.youSendValue}
                    teamGetsValue={gameTableStore.teamGetsValue}
                    onYouSendInput={onYouSendChange}
                    onTeamGetsInput={onTeamGetsChange}
                    isFullInfoShown={isFullInfoShown}
                    onClickShowFullInfo={() => setIsFullInfoShown(true)}
                    rateTable={gameStore.rateTable}
                  />
                ),
                pathname: ERoutes.TABLE_CALCULATE,
                title: 'Calculate your rate',
              },
            ]}
            overlayRef={contentRef}
            defaultPathname={ERoutes.TABLE_RULES}
          />
        </Col>
        <Col flex={'auto'} ref={contentRef}></Col>
      </Row>
    </div>
  );
});
