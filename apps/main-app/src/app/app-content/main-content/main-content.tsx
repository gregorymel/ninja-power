import { RightBanner } from './right-banner';
import React from 'react';
import LeftBannerController from '../../left-banner-controller/left-banner-controller';
import PlaygroundController from '../../playground-controller/playground-controller';

export const MainContent = () => {
  return (
    <>
      <PlaygroundController />
      <LeftBannerController />
      <RightBanner />
    </>
  );
};
