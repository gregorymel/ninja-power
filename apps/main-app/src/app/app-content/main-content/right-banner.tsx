import { Col, Row, Tooltip, Typography } from 'antd';
import { Icon } from '@ninja-power/ui-components';
import { InfoIcon } from '@ninja-power/ui-icons';
import styles from './right-banner.module.less';

export const RightBanner = () => {
  return (
    <div className={styles['right-banner-container']}>
      <Row>
        <Typography.Text>100% money back</Typography.Text>
      </Row>
      <Row>
        <Tooltip
          title={
            <div>
              During Promo period losing team players get all money back, win
              team players get all money back + part of prize in proportion to
              the contribution to the victory
            </div>
          }
        >
          <Row align={'middle'} gutter={4} wrap={false} justify={'end'}>
            <Col>
              <Icon icon={<InfoIcon />} size={'small'} />
            </Col>
            <Col>
              <Typography.Text type={'secondary'}>Promo period</Typography.Text>
            </Col>
          </Row>
        </Tooltip>
      </Row>
    </div>
  );
};
