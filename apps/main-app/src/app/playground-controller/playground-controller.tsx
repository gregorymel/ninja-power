import Playground from '../playground/playground';
import React from 'react';
import { useStore } from '@ninja-power/controllers';
import { observer } from 'mobx-react-lite';
import { notification } from 'antd';

/* eslint-disable-next-line */
export interface PlaygroundControllerProps {}

export const PlaygroundController = observer(
  (props: PlaygroundControllerProps) => {
    const {
      whiteTeamStore,
      blackTeamStore,
      gameStore,
      appController,
      web3Store,
    } = useStore();

    return (
      <Playground
        scorePercent={
          (blackTeamStore.points - whiteTeamStore.points) /
          (blackTeamStore.points + whiteTeamStore.points)
        }
        bagAmount={appController.bagAmount}
        whiteRockProps={{
          inputProps: {
            value: whiteTeamStore.inputStore.value,
            onInput: (e) => {
              whiteTeamStore.inputStore.onInput(e);
              whiteTeamStore.updatePlusPoints();
            },
            disabled: !web3Store.isMetamaskInstalled,
          },
          isRightSide: false,
          points: whiteTeamStore.points,
          yourPoints: whiteTeamStore.yourPoints,
          plusPointsForTeam: whiteTeamStore.plusPoints,
          pointsForTeamVisibility: !!whiteTeamStore.inputStore.value,
          sendButtonProps: {
            onClick: async () => {
              try {
                await gameStore.onVote(whiteTeamStore.inputStore.value, true);
                whiteTeamStore.inputStore.clear();
                notification.success({ message: 'Transaction successful' });
                await gameStore.fetchGameState();
              } catch (e) {
                if (e instanceof Error) {
                  notification.error({
                    message: `Something went wrong`,
                    description: e.message,
                  });
                } else {
                  notification.error({
                    message: `Something went wrong`,
                    description: String(e),
                  });
                }
              }
            },
            disabled: !whiteTeamStore.inputStore.value,
            loading: gameStore.isVoting,
          },
          inputTooltipTitle: web3Store.isMetamaskInstalled
            ? ''
            : 'You should install Metamask to participate',
        }}
        blackRockProps={{
          inputProps: {
            value: blackTeamStore.inputStore.value,
            onInput: (e) => {
              blackTeamStore.inputStore.onInput(e);
              blackTeamStore.updatePlusPoints();
            },
            disabled: !web3Store.isMetamaskInstalled,
          },
          isRightSide: true,
          points: blackTeamStore.points,
          yourPoints: blackTeamStore.yourPoints,
          plusPointsForTeam: blackTeamStore.plusPoints,
          pointsForTeamVisibility: !!blackTeamStore.inputStore.value,
          sendButtonProps: {
            onClick: async () => {
              try {
                await gameStore.onVote(blackTeamStore.inputStore.value, false);
                blackTeamStore.inputStore.clear();
                notification.success({ message: 'Transaction successful' });
                await gameStore.fetchGameState();
              } catch (e) {
                if (e instanceof Error) {
                  notification.error({
                    message: `Something went wrong`,
                    description: e.message,
                  });
                } else {
                  notification.error({
                    message: `Something went wrong`,
                    description: String(e),
                  });
                }
              }
            },
            disabled: !blackTeamStore.inputStore.value,
            loading: gameStore.isVoting,
          },
          inputTooltipTitle: web3Store.isMetamaskInstalled
            ? ''
            : 'You should install Metamask to participate',
        }}
      />
    );
  },
);

export default PlaygroundController;
