import { BlacksImage, WhitesImage } from '@ninja-power/ui-icons';
import { HTMLProps } from 'react';

interface Props extends HTMLProps<SVGSVGElement> {
  isBlacks?: boolean;
}

export const Ninjas = ({ isBlacks, ...rest }: Props) => {
  return isBlacks ? <BlacksImage {...rest} /> : <WhitesImage {...rest} />;
};
