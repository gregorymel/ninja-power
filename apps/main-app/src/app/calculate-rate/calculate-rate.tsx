import styles from './calculate-rate.module.less';
import { Button, Col, Input, Row, Table, Tooltip, Typography } from 'antd';
import { ColumnType } from 'antd/es/table/interface';
import React from 'react';
import { IRateTableRow } from '@ninja-power/controllers';
import { DateTime } from 'luxon';
import { currencyName } from '@ninja-power/config';

interface DataItem {
  time: string;
  value: string;
  startDateTime: DateTime;
  endDateTime: DateTime;
}

const columns: ColumnType<DataItem>[] = [
  {
    title: 'Time range',
    key: 'time',
    dataIndex: 'time',
    render: (value, record) => (
      <Tooltip
        title={record?.startDateTime?.toRelativeCalendar({
          locale: 'en',
          unit: 'hours',
        })}
      >
        {value}
      </Tooltip>
    ),
  },
  {
    title: '',
    key: 'value',
    dataIndex: 'value',
  },
];

const SHORT_ROWS_COUNT = 10;

export interface CalculateRateProps {
  youSendValue: string;
  onYouSendInput: (newValue: string) => void;
  teamGetsValue: string;
  onTeamGetsInput: (newValue: string) => void;
  isFullInfoShown: boolean;
  onClickShowFullInfo: () => void;
  rateTable: IRateTableRow[];
}

export const CalculateRate = ({
  youSendValue,
  teamGetsValue,
  onYouSendInput,
  onTeamGetsInput,
  onClickShowFullInfo,
  isFullInfoShown,
  rateTable,
}: CalculateRateProps) => {
  const fakeDataSource = rateTable.map(
    ({ startDateTime, endDateTime, value, t1, t2 }) => {
      const time = `${t1.toFormat('HH:mm')} – ${t2.toFormat('HH:mm')}`;

      return {
        time,
        value: `x${value}`,
        key: time,
        startDateTime,
        endDateTime,
      };
    },
  );

  const isShowMoreButtonVisible =
    !isFullInfoShown && rateTable.length > SHORT_ROWS_COUNT;

  return (
    <>
      <Row gutter={32}>
        <Col flex={'1'}>
          <Typography.Text>You send:</Typography.Text>
        </Col>
        <Col flex={'1'}>
          <Typography.Text>Team gets:</Typography.Text>
        </Col>
      </Row>
      <Row gutter={32}>
        <Col flex={'1'}>
          <Input
            bordered
            size={'large'}
            suffix={currencyName}
            value={youSendValue}
            onInput={({ currentTarget: { value } }) => onYouSendInput(value)}
          />
        </Col>
        <Col flex={'1'}>
          <Input
            bordered
            size={'large'}
            suffix={'Points'}
            value={teamGetsValue}
            onInput={({ currentTarget: { value } }) => onTeamGetsInput(value)}
          />
        </Col>
      </Row>
      <Row className={styles['table-container']}>
        <Col flex={'auto'}>
          <Table
            className={styles['table']}
            dataSource={
              isFullInfoShown
                ? fakeDataSource
                : fakeDataSource.slice(0, SHORT_ROWS_COUNT)
            }
            columns={columns}
            pagination={false}
            tableLayout={'fixed'}
            onRow={(record) => {
              const isCurrent =
                +record.startDateTime.diffNow('seconds') < 0 &&
                +record.endDateTime.diffNow('seconds') > 0;

              return {
                className: isCurrent ? styles['current'] : '',
              };
            }}
          />
        </Col>
      </Row>
      {isShowMoreButtonVisible && (
        <Row>
          <Col>
            <Button onClick={onClickShowFullInfo}>Show more</Button>
          </Col>
        </Row>
      )}
    </>
  );
};

export default CalculateRate;
