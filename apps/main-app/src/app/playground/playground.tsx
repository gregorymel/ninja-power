import styles from './playground.module.less';
import React from 'react';
import { BagImage, NinjasImage } from '@ninja-power/ui-icons';
import classNames from 'classnames';
import { ConfigProvider, Typography } from 'antd';
import RockTeamStats, {
  RockTeamStatsProps,
} from '../rock-team-stats/rock-team-stats';
import { digitSplitter } from '@ninja-power/utils';
import { currencyName } from '@ninja-power/config';

export interface PlaygroundProps {
  bagAmount: number;
  scorePercent: number;
  whiteRockProps: RockTeamStatsProps;
  blackRockProps: RockTeamStatsProps;
}

export function Playground({
  bagAmount,
  scorePercent,
  whiteRockProps,
  blackRockProps,
}: PlaygroundProps) {
  const isWhiteWins = scorePercent < 0;
  const isBlackWins = scorePercent > 0;
  const isPat = scorePercent === 0;
  const isBagEmpty = bagAmount <= 0;
  const isBagSmall = bagAmount > 0 && bagAmount < 100;
  const isBagMedium = bagAmount >= 100 && bagAmount < 1000;
  const isBagLarge = bagAmount >= 1000 && bagAmount < 10000;
  const isBagExtraLarge = bagAmount >= 10000 && bagAmount < 100000;
  const isBag2ExtraLarge = bagAmount >= 100000;
  const translateXValue = (
    (2 / (1 + Math.exp(-scorePercent)) - 1) *
    160
  ).toFixed(2);

  return (
    <div className={styles['container']}>
      <div className={styles['ninjas']}>
        <NinjasImage />
        <div
          className={classNames(
            styles['bag'],
            isBagSmall && styles['bag-size-s'],
            isBagMedium && styles['bag-size-m'],
            isBagLarge && styles['bag-size-l'],
            isBagExtraLarge && styles['bag-size-xl'],
            isBag2ExtraLarge && styles['bag-size-2xl'],
            isBlackWins && styles['bag-black'],
            isWhiteWins && styles['bag-white'],
          )}
          style={{
            transform: `translateY(-40%) translateX(${translateXValue}%)`,
          }}
        >
          <div className={styles['bag-svg']}>
            <BagImage />
          </div>
          <ConfigProvider
            theme={{
              components: {
                Typography: {
                  colorText: isWhiteWins ? '#000' : '#fff',
                },
              },
            }}
          >
            <Typography.Paragraph className={styles['bag-text']}>
              {digitSplitter(bagAmount.toFixed(2))}
            </Typography.Paragraph>
            {!isBagEmpty && (
              <Typography.Paragraph className={styles['bag-text-coins']}>
                {currencyName}
              </Typography.Paragraph>
            )}
          </ConfigProvider>
        </div>
        <RockTeamStats {...whiteRockProps} />
        <RockTeamStats {...blackRockProps} />
      </div>
    </div>
  );
}

export default Playground;
