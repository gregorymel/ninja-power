export enum ERoutes {
  MAIN = '/',
  TABLE = '/table',
  TABLE_RULES = '/table/rules',
  TABLE_WINNERS = '/table/winners',
  TABLE_WITHDRAW = '/table/withdraw',
  TABLE_CALCULATE = '/table/calculate',
  METAMASK_NOT_INSTALLED = '/metamask-not-installed',
}
