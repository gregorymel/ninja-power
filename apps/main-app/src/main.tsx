import * as ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';

import App from './app/app';
import { ConfigProvider } from 'antd';
import { theme } from './theme';
import * as Sentry from '@sentry/react';
import { applyTimeNowChange } from '@ninja-power/utils';

applyTimeNowChange();

Sentry.init({
  dsn: 'https://2faf099318d840c68c7ba975a5126432@o536317.ingest.sentry.io/4505308843999232',
  integrations: [
    new Sentry.BrowserTracing({
      // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
      tracePropagationTargets: ['localhost'],
    }),
    new Sentry.Replay(),
  ],
  // Performance Monitoring
  tracesSampleRate: 0.5,
});

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
);
root.render(
  <BrowserRouter>
    <ConfigProvider theme={theme}>
      <App />
    </ConfigProvider>
  </BrowserRouter>,
);
