FROM node:18.13.0 as builder
WORKDIR /app

COPY package.json .
COPY package-lock.json .
COPY .npmrc .
RUN npm ci
COPY apps/main-app apps/main-app
COPY libs libs
COPY tools tools
COPY tsconfig.base.json .
COPY add-ts-nocheck.js add-ts-nocheck.js
RUN node add-ts-nocheck.js
RUN npx nx run main-app:build:production

FROM nginx:1.16.0-alpine

WORKDIR /usr/share/nginx/html
RUN rm -rf ./*

COPY ./default.conf /etc/nginx/conf.d
COPY --from=builder /app/dist/apps/main-app /usr/share/nginx/html

EXPOSE 80 443
ENTRYPOINT ["nginx", "-g", "daemon off;"]

