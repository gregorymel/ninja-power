import { Link, useLocation } from 'react-router-dom';
import { HTMLProps, PropsWithChildren, PropsWithoutRef } from 'react';

export interface LinkWithQueryProps
  extends PropsWithoutRef<PropsWithChildren<HTMLProps<HTMLAnchorElement>>> {
  to: string;
  external?: boolean;
}

export function LinkWithQuery({ to, external, ...rest }: LinkWithQueryProps) {
  const { search } = useLocation();

  return <Link to={external ? to : to + search} {...rest} />;
}

export default LinkWithQuery;
