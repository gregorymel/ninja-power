import { PropsWithChildren } from 'react';
import { FollowIcon } from '@ninja-power/ui-icons';
import styles from './external-link.module.less';

export interface ExternalLinkProps extends PropsWithChildren {
  href: string;
}

export function ExternalLink({ children, href }: ExternalLinkProps) {
  return (
    <a
      target="_blank"
      rel="noreferrer"
      href={href}
      className={styles['container']}
    >
      {children}&nbsp;
      <FollowIcon className={styles['icon']} />
    </a>
  );
}

export default ExternalLink;
