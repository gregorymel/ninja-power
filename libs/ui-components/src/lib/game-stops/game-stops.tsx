import styles from './game-stops.module.less';
import { Typography } from 'antd';
import { Duration } from 'luxon';
import { HTMLProps } from 'react';
import classNames from 'classnames';

export interface GameStopsProps extends HTMLProps<HTMLDivElement> {
  stopsInDuration: Duration;
  isCompact?: boolean;
  gameId: number;
}

export function GameStops({
  stopsInDuration,
  isCompact = true,
  className,
  gameId,
  ...rest
}: GameStopsProps) {
  return (
    <div className={classNames(styles['container'], className)} {...rest}>
      <div>
        <Typography.Text type={'secondary'}>
          Game #{gameId} stops in:
        </Typography.Text>
      </div>
      <Typography.Title
        level={isCompact ? 3 : 1}
        className={styles['elapsed-time']}
      >
        {stopsInDuration.toFormat('hh:mm:ss')}
      </Typography.Title>
    </div>
  );
}

export default GameStops;
