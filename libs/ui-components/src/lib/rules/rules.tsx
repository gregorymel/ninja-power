import styles from './rules.module.less';
import { ConfigProvider, Divider, Typography } from 'antd';
import { currencyName } from '@ninja-power/config';
import RuleStatement from '../rule-statement/rule-statement';
import LinkWithQuery from '../link-with-query/link-with-query';

export interface RulesProps {
  multiplierCoefLink: string;
}

export function Rules({ multiplierCoefLink }: RulesProps) {
  return (
    <ConfigProvider
      theme={{
        components: {
          Button: {
            paddingContentHorizontal: 0,
          },
        },
      }}
    >
      <div className={styles['container']}>
        <Typography.Title level={4} className={styles['promo-period-text']}>
          Promo period - guaranteed 100% money back for all players!
        </Typography.Title>
        <RuleStatement
          title={'Description:'}
          description={
            <>
              <Typography.Paragraph>
                Ninja Power - true web3 game with simple rules and transparent
                results.
              </Typography.Paragraph>
              <Typography.Paragraph>
                There are two teams - White & Black. Team which collects more
                points during 24 hours - wins.
              </Typography.Paragraph>
            </>
          }
        />
        <Divider />
        <RuleStatement
          title={'How to play:'}
          description={
            <>
              <Typography.Paragraph>
                Points are awarded for sending {currencyName} tokens. Amount of
                points = amount of {currencyName} * Multiplier coefficient.{' '}
                <LinkWithQuery to={multiplierCoefLink}>
                  Multiplier coefficient{' '}
                </LinkWithQuery>
                decreases in time.{' '}
                <LinkWithQuery to={multiplierCoefLink}>Calculate</LinkWithQuery>
              </Typography.Paragraph>
              <Typography.Paragraph>
                <ul>
                  <li>Connect your METAMASK</li>
                  <li>Decide which team do you want to play with</li>
                  <li>
                    Claim CORE tokens here:{' '}
                    <a
                      href="https://scan.test.btcs.network/faucet"
                      target={'_blank'}
                      rel={'noreferrer noopener'}
                    >
                      https://scan.test.btcs.network/faucet
                    </a>
                  </li>
                  <li>Send {currencyName} on chosen team’s token address</li>
                </ul>
              </Typography.Paragraph>
            </>
          }
        />
        <Divider />
        <RuleStatement
          title={'Win:'}
          description={
            <>
              <Typography.Paragraph>
                Each player from the winning team gets his token back +
                a&nbsp;part of&nbsp;tokens from the lost team, which
                is&nbsp;proportional to&nbsp;the players&rsquo; contribution
                (in&nbsp;terms of&nbsp;points) to&nbsp;the win
              </Typography.Paragraph>
              <Typography.Paragraph>
                Maintenance fee of&nbsp;the game&nbsp;&mdash; 1%
              </Typography.Paragraph>
            </>
          }
        />
      </div>
    </ConfigProvider>
  );
}

export default Rules;
