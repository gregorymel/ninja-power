import styles from './icon.module.less';
import React from 'react';
import classnames from 'classnames';

export interface IconProps {
  icon: React.ReactNode;
  size?: 'medium' | 'large' | 'small';
  shadow?: boolean;
}

export function Icon({ icon, size = 'medium', shadow }: IconProps) {
  return (
    <div
      className={classnames(styles['container'], styles[size], {
        [styles['shadow']]: shadow,
      })}
    >
      {icon}
    </div>
  );
}

export default Icon;
