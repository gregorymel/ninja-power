import { createContext } from 'react';
import { GameStore } from './app-store/game-store';
import { Web3Store } from './app-store/web3-store';
import { InitialLoadingStore } from './app-store/initial-loading-store';
import { TeamStore } from './app-store/team-store';
import { configure } from 'mobx';
import { ChainStore } from './app-store/chain-store';
import { GameTableStore } from './app-store/game-table-store';
import { AppController } from './controllers/app-controller';
import { WinnersStore } from './winners-store/winners-store';
import { LocalStorageStore } from './local-storage-store/local-storage-store';

configure({
  enforceActions: 'never',
});

export class Store {
  readonly whiteTeamStore: TeamStore;
  readonly blackTeamStore: TeamStore;
  readonly gameStore: GameStore;
  readonly web3Store: Web3Store;
  readonly initialLoadingStore: InitialLoadingStore;
  readonly chainStore: ChainStore;
  readonly gameTableStore: GameTableStore;
  readonly winnersStore: WinnersStore;
  readonly localStorageStore: LocalStorageStore;

  readonly appController: AppController;

  constructor() {
    this.chainStore = new ChainStore();
    this.web3Store = new Web3Store(this.chainStore);
    this.gameStore = new GameStore(this.web3Store);
    this.whiteTeamStore = new TeamStore(this.gameStore);
    this.blackTeamStore = new TeamStore(this.gameStore);
    this.gameTableStore = new GameTableStore(this.gameStore);
    this.initialLoadingStore = new InitialLoadingStore();
    this.localStorageStore = new LocalStorageStore();
    this.winnersStore = new WinnersStore(
      this.localStorageStore,
      this.chainStore,
      this.gameStore,
    );

    this.appController = new AppController(
      this.whiteTeamStore,
      this.blackTeamStore,
      this.gameStore,
      this.chainStore,
      this.web3Store,
      this.initialLoadingStore,
    );
  }
}

export const StoreContext = createContext(new Store());
