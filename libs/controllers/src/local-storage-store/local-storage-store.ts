import { makeAutoObservable } from 'mobx';
import { ChainService } from '../services/chain-service';
import { EChain } from '../types/enums';

const localStorageChainMapping = {
  [EChain.CORE_TESTNET]: 'core',
};
const LOCAL_STORAGE_CACHE_KEY =
  localStorageChainMapping[ChainService.allowedChain];

export class LocalStorageStore {
  data = localStorage.getItem(LOCAL_STORAGE_CACHE_KEY)
    ? JSON.parse(localStorage.getItem(LOCAL_STORAGE_CACHE_KEY))
    : {};
  constructor() {
    makeAutoObservable(this);
  }

  getProp = (name: string, defaultVal: any = null) => {
    return this.data[name] || defaultVal;
  };

  setProp = (name: string, val: any) => {
    this.data[name] = val;
    this.save();
  };

  save = () => {
    localStorage.setItem(LOCAL_STORAGE_CACHE_KEY, JSON.stringify(this.data));
  };
}
