export const objectReverse = <
  K extends string | number | symbol,
  V extends string | number | symbol,
>(
  obj: Record<K, V>,
): Record<V, K> => {
  const result = {} as Record<V, K>;

  for (const key in obj) {
    result[obj[key]] = key;
  }

  return result;
};
