declare let window: any;

import { EChain, EWeb3Error } from '../types/enums';
import {
  CHAIN_BLOCK_EXPLORER_URLS,
  CHAIN_IDS,
  CHAIN_NAMES,
  CHAIN_RPC_URLS,
} from '../types/constants';
import { objectReverse } from '../utils/object-reverse';

export class ChainService {
  static allowedChain = EChain.CORE_TESTNET;

  static async getMetamaskCurrentChainId() {
    return await window?.ethereum?.request({
      method: 'eth_chainId',
    });
  }

  static async changeChain(chain: EChain) {
    await ChainService.changeMetamaskChain(chain);
  }

  private static async changeMetamaskChain(chain: EChain) {
    try {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: ChainService.chainIdHex(CHAIN_IDS[chain]) }],
      });
    } catch (error: any) {
      if (error.code === EWeb3Error.CHAIN_NOT_ADDED) {
        await ChainService.addMetamaskChain(chain);
        return;
      }

      throw new Error(error);
    }
  }

  private static async addMetamaskChain(chain: EChain) {
    const error = await window.ethereum.request({
      method: 'wallet_addEthereumChain',
      params: [
        {
          chainId: ChainService.chainIdHex(CHAIN_IDS[chain]),
          chainName: CHAIN_NAMES[chain],
          nativeCurrency: {
            name: 'Ethereum',
            symbol: 'ETH', // 2-6 characters long
            decimals: 18,
          },
          rpcUrls: [CHAIN_RPC_URLS[chain]],
          blockExplorerUrls: [CHAIN_BLOCK_EXPLORER_URLS[chain]],
        },
      ],
    });

    if (!error) {
      return;
    }

    throw new Error(error);
  }

  public static chainIdHex(chainId: number) {
    return '0x' + chainId.toString(16);
  }

  static getKnownChainById(chainId?: number): EChain | undefined {
    if (!chainId) {
      return undefined;
    }
    return objectReverse(CHAIN_IDS)[chainId];
  }

  static fixCurrentChain = async () => {
    let changed = false;
    let currentChainId = await ChainService.getMetamaskCurrentChainId();
    currentChainId = parseInt(currentChainId, 16);

    if (currentChainId !== CHAIN_IDS[ChainService.allowedChain]) {
      try {
        await ChainService.changeChain(ChainService.allowedChain);
        changed = true;
      } catch (e) {
        console.error(e);
      }
    }

    return changed;
  };
}
