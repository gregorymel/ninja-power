export enum ELocalStorage {
  lastLoginAddress = 'last-login-address',
}

export type TLocalStorage = keyof typeof ELocalStorage;

export class LocalStorageService {
  static get(key: TLocalStorage): string | null {
    return localStorage.getItem(ELocalStorage[key]);
  }

  static set(key: TLocalStorage, value: any): void {
    localStorage.setItem(ELocalStorage[key], value);
  }

  static remove(key: TLocalStorage): void {
    localStorage.removeItem(ELocalStorage[key]);
  }

  static clear(): void {
    localStorage.clear();
  }
}
