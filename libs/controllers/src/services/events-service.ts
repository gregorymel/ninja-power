import { EventEmitter } from 'events';
import { Events } from '../types/events';

class EventsService extends EventEmitter {
  revalidateGameState = () => {
    this.emit(Events.GAME_STATE_REVALIDATE);
  };
}

export const eventsService = new EventsService();
