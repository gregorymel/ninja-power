import { makeAutoObservable } from 'mobx';
import * as React from 'react';

export class InputStore {
  value = '';

  constructor() {
    makeAutoObservable(this);
  }

  onInput = (e: React.FormEvent<HTMLInputElement>) => {
    this.value = e.currentTarget.value;
  };

  clear = () => {
    this.value = '';
  };
}
