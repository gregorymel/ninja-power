import { makeAutoObservable } from 'mobx';
import { WinnersItem } from '../types/winners-store';
import { Web3Service } from '../services/web3-service';
import {
  getContractsForChainOrThrow,
  TugOfWarExtended,
} from '@tug-of-war/contracts';
import TugOfWar_ABI from '@tug-of-war/contracts/dist/abi/TugOfWarV2.json';
import { ChainStore } from '../app-store/chain-store';
import { LocalStorageStore } from '../local-storage-store/local-storage-store';
import { Contract, ethers } from 'ethers';
import { GameStore } from '../app-store/game-store';
import { CHAIN_IDS } from '../types/constants';
import { ChainService } from '../services/chain-service';

export class WinnersStore {
  _winners: Record<number, WinnersItem> = {};
  lastGameResult: WinnersItem | null = null;

  constructor(
    private readonly localStorageStore: LocalStorageStore,
    private readonly chainStore: ChainStore,
    private readonly gameStore: GameStore,
  ) {
    makeAutoObservable(this);

    this._winners = this.localStorageStore.getProp('winners', {});
    if (
      Object.values(this._winners).some(
        (gameInfo) => !('whiteTokens' in gameInfo),
      )
    ) {
      this.localStorageStore.setProp('winners', {});
    }

    for (const gameIdx in this._winners) {
      this._winners[gameIdx].prize = Math.round(this._winners[gameIdx].prize);
      this._winners[gameIdx].whiteTokens = Math.round(
        this._winners[gameIdx].whiteTokens,
      );
      this._winners[gameIdx].blackTokens = Math.round(
        this._winners[gameIdx].blackTokens,
      );
    }
  }

  get winners() {
    return Object.values(this._winners)
      .sort((a, b) => a.number - b.number)
      .reverse();
  }

  updateLastGameResult = async () => {
    if (!Web3Service.contracts) {
      throw new Error('Web3Service.contracts is null');
    }
    const chainId = CHAIN_IDS[ChainService.allowedChain];

    const contracts = getContractsForChainOrThrow(
      chainId,
      Web3Service.provider,
    );
    const lastGameIdx =
      +(await contracts.gameFactoryContract.getCurrentGameIdx());

    const gameAddress = await contracts.gameFactoryContract.getAddress(
      lastGameIdx,
    );
    const game = new Contract(
      gameAddress,
      TugOfWar_ABI,
      Web3Service.provider,
    ) as TugOfWarExtended;

    let prize = 0;
    try {
      prize = +ethers.utils.formatUnits(await game.rewardAmount(), 18);
    } catch (e) {
      prize =
        +ethers.utils.formatUnits(await game.pool1TotalBalance(), 18) +
        +ethers.utils.formatUnits(await game.pool2TotalBalance(), 18);
    }

    this.lastGameResult = {
      number: lastGameIdx,
      white: +ethers.utils.formatUnits(await game.pool1Points(), 18),
      black: +ethers.utils.formatUnits(await game.pool2Points(), 18),
      prize,
    } as WinnersItem;
  };

  update = async () => {
    if (!Web3Service.contracts) {
      throw new Error('Web3Service.contracts is null');
    }
    const chainId = CHAIN_IDS[ChainService.allowedChain];

    const contracts = getContractsForChainOrThrow(
      chainId,
      Web3Service.provider,
    );

    const promises: Promise<WinnersItem>[] = [];
    for (let gameId = 1; gameId < this.gameStore.gameId; gameId++) {
      if (gameId in this._winners) {
        continue;
      }

      const promise = (async () => {
        const gameAddress = await contracts.gameFactoryContract.getAddress(
          gameId,
        );
        const game = new Contract(
          gameAddress,
          TugOfWar_ABI,
          Web3Service.provider,
        ) as TugOfWarExtended;
        return {
          number: gameId,
          white: +ethers.utils.formatUnits(await game.pool1Points(), 18),
          black: +ethers.utils.formatUnits(await game.pool2Points(), 18),
          prize: +ethers.utils.formatUnits(await game.rewardAmount(), 18),
          whiteTokens: +ethers.utils.formatUnits(
            await game.pool1TotalBalance(),
            18,
          ),
          blackTokens: +ethers.utils.formatUnits(
            await game.pool2TotalBalance(),
            18,
          ),
        } as WinnersItem;
      })();
      promises.push(promise);
    }
    const resolvedResults = await Promise.allSettled(promises);

    for (const settledPromise of resolvedResults.filter(
      (v) => v.status === 'fulfilled',
    )) {
      if (settledPromise.status === 'fulfilled') {
        const { value: gameInfo } = settledPromise;

        this._winners[gameInfo.number] = gameInfo;
        this.localStorageStore.setProp('winners', this._winners);
        this.localStorageStore.save();
      }
    }
  };
}
