import { makeAutoObservable } from 'mobx';

export class InitialLoadingStore {
  isLoading = true;
  isLoaderHidden = false;
  message?: string;

  constructor() {
    makeAutoObservable(this);
  }

  setIsLoaded = () => {
    this.isLoading = false;
  };
  hideLoader = () => {
    this.isLoaderHidden = true;
  };
  setMessage = (value: string) => {
    this.message = value;
  };
}
