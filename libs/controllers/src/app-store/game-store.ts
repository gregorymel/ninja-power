import { makeAutoObservable } from 'mobx';
import { DateTime, Duration } from 'luxon';
import { Web3Store } from './web3-store';
import { GameService } from '../services/game-service';
import { ethers } from 'ethers';
import { interval, noop, tap } from 'rxjs';
import { IRateTableRow } from '../types/game-store';
import { Web3Service } from '../services/web3-service';

const fakeDataSource = [
  {
    time: '00:05 – 00:03',
    value: 'x3',
    key: '1',
  },
  {
    time: '00:03 – 00:01',
    value: 'x2',
    key: '2',
  },
  {
    time: '00:01 – 00:00',
    value: 'x1',
    key: '3',
  },
];

const rateTable: IRateTableRow[] = [];

export class GameStore {
  stopsDateTime: DateTime | null = null;
  stopsInDuration: Duration = Duration.fromMillis(0);
  startsDateTime: DateTime | null = DateTime.now().startOf('day');
  isGameStopped = false;
  rateChangeIn = Duration.fromMillis(0);
  currentRateValue = 1;
  nextGameDateTime: DateTime | null = null;
  nextGameDuration: Duration | null = null;
  isVoting = false;
  isWithdrawButtonLoading = false;
  rateTable = rateTable;
  rewardAmount = 0;
  amountToWithdraw = 0;
  gameId = 1;

  constructor(readonly web3Store: Web3Store) {
    makeAutoObservable(this);

    interval(1000)
      .pipe(
        tap(() => {
          this.nextGameDuration = this.nextGameDateTime?.diffNow();
          if (this.stopsDateTime) {
            this.stopsInDuration = this.stopsDateTime.diffNow();
            this.isGameStopped = +this.stopsDateTime.diffNow('second') < 0;
          }
          this.updateCurrentRate();
        }),
      )
      .subscribe(noop);
  }

  fetchBaseContract = () => {
    return GameService.getCurrentGame();
  };

  fetchGameState = async () => {
    if (!Web3Service.contracts) {
      throw new Error('Web3Service.contracts is null');
    }
    const tugOfWar = await this.fetchBaseContract();

    const isActive = await tugOfWar.isActive();

    let poll1Points = 0;
    let poll2Points = 0;
    let endTime = 0;
    let myPoll1Balance = 0;
    let myPoll2Balance = 0;
    let myPoll1Points = 0;
    let myPoll2Points = 0;
    let poll1TotalBalance = 0;
    let poll2TotalBalance = 0;
    let rewardAmount = 0; // default value
    const gameId = (
      await Web3Service.contracts.gameFactoryContract.getCurrentGameIdx()
    ).toNumber();

    endTime = (
      await Web3Service.contracts.gameFactoryContract.getCurrentGameEndTime()
    ).toNumber();
    rewardAmount = poll1TotalBalance + poll2TotalBalance;
    if (isActive) {
      poll1Points = +ethers.utils.formatUnits(await tugOfWar.pool1Points(), 18);
      poll2Points = +ethers.utils.formatUnits(await tugOfWar.pool2Points(), 18);

      if (this.web3Store.currentAddress) {
        myPoll1Balance = +ethers.utils.formatUnits(
          await tugOfWar.pool1Balances(this.web3Store.currentAddress),
          18,
        );
        myPoll2Balance = +ethers.utils.formatUnits(
          await tugOfWar.pool2Balances(this.web3Store.currentAddress),
          18,
        );
        myPoll1Points = +ethers.utils.formatUnits(
          await tugOfWar.pool1PointsBalances(this.web3Store.currentAddress),
          18,
        );
        myPoll2Points = +ethers.utils.formatUnits(
          await tugOfWar.pool2PointsBalances(this.web3Store.currentAddress),
          18,
        );
      }

      poll1TotalBalance = +ethers.utils.formatUnits(
        await tugOfWar.pool1TotalBalance(),
        18,
      );
      poll2TotalBalance = +ethers.utils.formatUnits(
        await tugOfWar.pool2TotalBalance(),
        18,
      );
      rewardAmount = +ethers.utils.formatUnits(
        await tugOfWar.rewardAmount(),
        18,
      );
    }

    this.stopsDateTime = DateTime.fromSeconds(endTime);
    this.isGameStopped = +this.stopsDateTime.diffNow('second') < 0;
    this.updateStartsTime(this.stopsDateTime.minus({ minute: 6 }));
    this.rewardAmount = rewardAmount;
    this.gameId = gameId;

    this.nextGameDateTime = this.stopsDateTime.plus({ minute: 1 });

    return {
      poll1Points,
      poll2Points,
      myPoll1Points,
      myPoll2Points,
      myPoll1Balance,
      myPoll2Balance,
      poll1TotalBalance,
      poll2TotalBalance,
    };
  };

  onVote = async (amount: string, isWhiteTeam: boolean) => {
    this.isVoting = true;
    try {
      if (!Web3Service.contracts) {
        throw new Error('Web3Service.contracts is null');
      }
      const tugOfWar = await this.fetchBaseContract();
      const sendToPoolFn = isWhiteTeam
        ? Web3Service.contracts.gameFactoryContract.sendToPool1
        : Web3Service.contracts.gameFactoryContract.sendToPool2;

      const sendToPoolTx = await sendToPoolFn(ethers.utils.parseEther(amount), {
        value: ethers.utils.parseEther(amount),
      });
      await sendToPoolTx.wait();
    } catch (e) {
      throw e;
    } finally {
      this.isVoting = false;
    }
  };

  updateRateTable = () => {
    this.rateTable = fakeDataSource.map(({ key, value, time }) => {
      const [t1, t2] = time.split(' – ');
      const [t1hours, t1min] = t1.split(':');
      const [t2hours, t2min] = t2.split(':');

      const startDateTime = this.stopsDateTime?.minus({
        hour: +t1hours,
        minute: +t1min,
      });
      const endDateTime = this.stopsDateTime?.minus({
        hour: +t2hours,
        minute: +t2min,
      });

      return {
        startDateTime,
        endDateTime,
        t1: DateTime.fromFormat(t1, 'HH:mm'),
        t2: DateTime.fromFormat(t2, 'HH:mm'),
        value: +value.slice(1),
      };
    });
    this.updateCurrentRate();
  };

  updateStartsTime = (startsDateTime: DateTime) => {
    this.startsDateTime = startsDateTime;
    this.updateRateTable();
  };

  updateCurrentRate = () => {
    const i = this.rateTable.findIndex(
      (v) => +v.endDateTime.diffNow('seconds') > 0,
    );
    if (i !== -1) {
      this.currentRateValue = this.rateTable[i].value;
      this.rateChangeIn = this.rateTable[i].endDateTime.diffNow('seconds');
    }
  };

  updateAmountToWithdraw = async () => {
    this.isWithdrawButtonLoading = true;
    try {
      await this.fetchAmountToWithdraw();
    } catch (e) {
      throw e;
    } finally {
      this.isWithdrawButtonLoading = false;
    }
  };

  fetchAmountToWithdraw = async () => {
    if (!Web3Service.contracts) {
      throw new Error('Web3Service.contracts is null');
    }
    if (!this.web3Store.currentAddress) {
      throw new Error('this.web3Store.currentAddress is null');
    }

    const previewWithdrawalAmount =
      await Web3Service.contracts.gameFactoryContract.previewWithdrawal(
        this.web3Store.currentAddress,
      );
    this.amountToWithdraw = +ethers.utils.formatUnits(
      previewWithdrawalAmount,
      18,
    );
  };

  onWithdraw = async () => {
    this.isWithdrawButtonLoading = true;
    try {
      if (!Web3Service.contracts) {
        throw new Error('Web3Service.contracts is null');
      }

      if (!this.web3Store.currentAddress) {
        throw new Error('Cannot withdraw to undefined address');
      }

      const withdrawTx =
        await Web3Service.contracts.gameFactoryContract.withdraw(
          this.web3Store.currentAddress,
        );
      await withdrawTx.wait();
    } catch (e) {
      throw e;
    } finally {
      this.isWithdrawButtonLoading = false;
    }
  };
}
