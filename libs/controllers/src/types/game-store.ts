import { DateTime } from 'luxon';

export interface IRateTableRow {
  startDateTime: DateTime;
  endDateTime: DateTime;
  t1: DateTime;
  t2: DateTime;
  value: number;
}
