export enum EChain {
  CORE_TESTNET = 'CoreTestnet',
}

export enum EWeb3Error {
  CHAIN_NOT_ADDED = 4902,
}
