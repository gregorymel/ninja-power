import { ChainId } from '@tug-of-war/contracts';
import { EChain } from './enums';

export const CHAIN_IDS: Record<EChain, number> = {
  [EChain.CORE_TESTNET]: ChainId.CoreTestnet,
};
export const CHAIN_NAMES: Record<EChain, string> = {
  [EChain.CORE_TESTNET]: 'Core Testnet',
};

export const CHAIN_RPC_URLS = {
  [EChain.CORE_TESTNET]: 'https://rpc.test.btcs.network',
};

export const CHAIN_BLOCK_EXPLORER_URLS = {
  [EChain.CORE_TESTNET]: 'https://scan.test.btcs.network',
};
