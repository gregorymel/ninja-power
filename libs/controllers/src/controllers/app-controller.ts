import { notification } from 'antd';

declare let window: any;

import { TeamStore } from '../app-store/team-store';
import { GameStore } from '../app-store/game-store';
import {
  debounceTime,
  EMPTY,
  fromEvent,
  interval,
  merge,
  noop,
  switchMap,
  tap,
} from 'rxjs';
import { eventsService } from '../services/events-service';
import { Events } from '../types/events';
import { ChainStore } from '../app-store/chain-store';
import { Web3Store } from '../app-store/web3-store';
import { InitialLoadingStore } from '../app-store/initial-loading-store';

const UPDATE_INTERVAL = 5000;

export class AppController {
  bagAmount = 0;
  isRevalidationIntervalRun = false;

  constructor(
    private readonly whiteTeamStore: TeamStore,
    private readonly blackTeamStore: TeamStore,
    private readonly gameStore: GameStore,
    private readonly chainStore: ChainStore,
    private readonly web3Store: Web3Store,
    private readonly initialLoadingStore: InitialLoadingStore,
  ) {
    fromEvent(eventsService, Events.GAME_STATE_LOGGED_IN)
      .pipe(
        switchMap(() => {
          if (!this.isRevalidationIntervalRun) {
            this.isRevalidationIntervalRun = true;

            return merge(
              interval(UPDATE_INTERVAL),
              fromEvent(eventsService, Events.GAME_STATE_REVALIDATE),
            ).pipe(
              tap(() => {
                this.revalidateGameState();
              }),
              debounceTime(UPDATE_INTERVAL),
            );
          }

          return EMPTY;
        }),
      )
      .subscribe(noop);
  }

  revalidateGameState = async () => {
    const {
      poll1Points,
      myPoll1Points,
      myPoll2Points,
      poll2Points,
      poll1TotalBalance,
      poll2TotalBalance,
      myPoll2Balance,
      myPoll1Balance,
    } = await this.gameStore.fetchGameState();
    this.whiteTeamStore.points = poll1Points;
    this.blackTeamStore.points = poll2Points;
    this.whiteTeamStore.yourPoints = myPoll1Points;
    this.blackTeamStore.yourPoints = myPoll2Points;
    this.whiteTeamStore.totalBalance = poll1TotalBalance;
    this.blackTeamStore.totalBalance = poll2TotalBalance;
    this.whiteTeamStore.yourBalance = myPoll1Balance;
    this.blackTeamStore.yourBalance = myPoll2Balance;

    this.whiteTeamStore.updatePointsShare();
    this.blackTeamStore.updatePointsShare();

    this.bagAmount = this.gameStore.rewardAmount;
  };

  init = async () => {
    if (window.ethereum) {
      window.ethereum.on('chainChanged', this.chainStore.onChainChanged);
      window.ethereum.on('accountsChanged', this.web3Store.onAccountChanged);
    }

    try {
      await this.web3Store.init();
      await this.gameStore.fetchBaseContract();
      await this.revalidateGameState();
      eventsService.emit(Events.GAME_STATE_LOGGED_IN);
      this.initialLoadingStore.setIsLoaded();
    } catch (e) {
      notification.error({
        message: 'Something went wrong',
        description: String(e),
      });
      this.initialLoadingStore.hideLoader();
      this.initialLoadingStore.setMessage(
        'Some error occurred, please try refreshing the page later',
      );
    }
  };
}
