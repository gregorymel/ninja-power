import { DateTime, Settings } from 'luxon';

export const applyTimeNowChange = () => {
  const search = new URLSearchParams(document.location.search);

  if (search.has('plus-minutes')) {
    Settings.now = () =>
      +DateTime.fromJSDate(new Date())
        .plus({ minute: +search.get('plus-minutes') })
        .toJSDate();
  }
};
