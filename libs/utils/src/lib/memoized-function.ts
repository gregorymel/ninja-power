import objectHash from 'object-hash';

export const memoizedFunction = <ArgType, R>(
  fn: (...args: ArgType[]) => R,
): ((...args: ArgType[]) => R) => {
  const hashes: Record<string, R> = {};

  return (...args: ArgType[]) => {
    const argsHash = objectHash(args);
    if (argsHash in hashes) {
      return hashes[argsHash];
    }

    const value = fn.call(this, ...args);
    hashes[argsHash] = value;

    return value;
  };
};
