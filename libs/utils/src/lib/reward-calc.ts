export const calcReward = (
  whitePoints: number,
  blackPoints: number,
  whiteTokens: number,
  blackTokens: number,
  userWhitePoints: number,
  userBlackPoints: number,
) => {
  const losingTeamTokens =
    whitePoints < blackPoints ? whiteTokens : blackTokens;
  const share =
    whitePoints > blackPoints
      ? userWhitePoints / whitePoints
      : userBlackPoints / blackPoints;

  const reward = 0.99 * losingTeamTokens * share;
  return reward;
};
